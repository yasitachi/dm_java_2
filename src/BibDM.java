import java.util.Collections;
import java.util.List;
import java.util.ArrayList;



public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){

    return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){

      if (liste.size()==0) {
        return null;}
      else{
        Integer mini= liste.get(0);
        for (Integer i : liste){
	      {if (i < mini) ;
	      mini = i ;
      }}
    return mini;
    }}


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */

    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){

      if (liste.size()==0){
        return true;}

      Collections.sort(liste);
      boolean res = false;
      if (valeur.compareTo(liste.get(0)) < 0)
    	res = true;
    return res;
    }


    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
     public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){

       List<T> res = new ArrayList<T>();
        int i1,i2;
        i1=0;
        i2=0;
        if (liste1.size() == 0 | liste2.size() == 0){
          return res;
        } else if (liste1.equals(liste2)){
          res = liste1;
          return res;
        } else {
          while (i1 <liste1.size() && i2 < liste2.size()){
            if(liste1.get(i1).compareTo(liste2.get(i2)) < 0){
              i1 +=1;
            } else if (liste1.get(i1).compareTo(liste2.get(i2)) == 0){
              res.add(liste1.get(i1));
              i1 +=1;
              i2 +=1;
            } else {
              i2 +=1;
            }
          }
        List<T> liste = new ArrayList<T>();

        for (T mot : res ){
          if (!liste.contains(mot)){
            liste.add(mot);
          }
        }
        res = liste;

        return res;

    }}


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){

      if(texte.equals("")) {

        List<String> liste = new ArrayList<String>();
          return liste;
      }

       String space = texte.replaceAll("\\s+", " ");
       List<String> listeMots = new ArrayList<>();
       String s = space.trim();
       String[] Mots = s.split(" ");
       for (String mot : Mots){
         listeMots.add(mot);
       }
    return listeMots;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

      int compteur1=0;
      int compteur2=0;
      String prec=null;
      String motPlusLong="";

      if (texte.length()==0){
        return null;
      }

      List<String> liste = decoupe(texte);
      Collections.sort(liste);

      for(String mot :liste){
        if(prec==null){
          prec=mot;
          compteur1+=1;}
        else{
          if(prec.equals(mot)){
            compteur1+=1;
          }
          else{
            if(compteur1>compteur2){
              motPlusLong=prec;
              compteur2=compteur1;}
            prec=mot;
            compteur1=0;
       }}}
    return motPlusLong;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){

      int compteur= 0;
      if(chaine.length()==0){
        return true;}
      else if(chaine.charAt(0)==')' || chaine.charAt(chaine.length()-1)=='('){
        return false;}
         else{
           for(int i=0; i<chaine.length(); ++i){
             if(chaine.charAt(i)=='('){
               compteur++;}
             else { compteur--;}
           }
        return compteur==0;
         }
    }


        /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
     public static boolean bienParentheseeCrochets(String chaine){

       int compteur1= 0;
       int compteur2= 0;

       if(chaine.length()==0){
          return true;
        }
        else if(chaine.charAt(0)==')' || chaine.charAt(0)==']' || chaine.charAt(chaine.length()-1)=='[' || chaine.charAt(chaine.length()-1)=='('){
          return false;
        }
        else{
          char prec=' ';

        for(int i=0; i<chaine.length(); ++i){
          if(chaine.charAt(i)=='('){
            compteur1++;}
          else if(chaine.charAt(i)==')'){
            if(prec=='['){
              return false;}
            compteur1--;}
          else if(chaine.charAt(i)=='['){
            compteur2++;}
          else if(chaine.charAt(i)==']'){
            if(prec=='('){
              return false;}
              compteur2--;
            }
          prec=chaine.charAt(i);

       }
          return compteur1==0 && compteur2==0;
     }}




    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){

      boolean trouve = false;
      int debut = 0;
      int fin = liste.size()-1;

      while (!trouve &&  debut <=  fin){
        int milieu = ( debut+  fin) / 2;

        if (liste.get( milieu) == valeur){
          trouve = true;
        }
        else if (liste.get( milieu) > valeur)
          fin =  milieu -1;
         else
          debut =  milieu +1;
      }
      return trouve;
    }

  }
